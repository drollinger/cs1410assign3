/*
Name: Dallin Drollinger
A#: A01984170

Description: Inside.java takes hard coded input from arrays with different (x, y) coordinates and
    specifications for circles and rectangles. It then prints the point info and whether it is found
    inside or outside the square or circle followed by info about the square or circle.
 */

public class Inside {
    public static void main(String[] args) {

        //Our hard coded info for our points, circles, and rectangles in arrays
        double[] ptX = { 1, 2, 3, 4 };
        double[] ptY = { 1, 2, 3, 4 };
        double[] circleX = { 0, 5 };
        double[] circleY = { 0, 5 };
        double[] circleRadius = { 3, 3 };
        double[] rectLeft = { -2.5, -2.5 };
        double[] rectTop = { 2.5, 5.0 };
        double[] rectWidth = { 6.0, 5.0 };
        double[] rectHeight = { 5.0, 2.5 };

        //Printing out info for the points in the circles
        System.out.println("--- Report of Points and Circles ---\n");
        //Our loop for iterating through each point through each circle
        for (int r = 0; r < circleRadius.length; r++) {
            for (int i = 0; i < ptX.length; i++) {
                reportPoint(ptX[i], ptY[i]);
                if (isPointInsideCircle(ptX[i], ptY[i], circleX[r], circleY[r], circleRadius[r]))
                    System.out.print(" is inside ");
                else
                    System.out.print(" is outside ");
                reportCircle(circleX[r], circleY[r], circleRadius[r]);
                System.out.println();
            }
        }

        //Printing out info for the points in the rectangles
        System.out.println("\n--- Report of Points and Rectangles ---\n");
        //Our loop for iterating through each point through each rectangle
        for (int r = 0; r < rectLeft.length; r++) {
            for (int i = 0; i < ptX.length; i++) {
                reportPoint(ptX[i], ptY[i]);
                if (isPointInsideRectangle(ptX[i], ptY[i], rectLeft[r], rectTop[r], rectWidth[r], rectHeight[r]))
                    System.out.print(" is inside ");
                else
                    System.out.print(" is outside ");
                reportRectangle(rectLeft[r], rectTop[r], rectWidth[r], rectHeight[r]);
                System.out.println();
            }
        }
    }

    //method for printing info about a point
    static void reportPoint(double x, double y) {
        System.out.printf("Point(%.1f, %.1f)", x, y);
    }

    //method for printing info about a circle
    static void reportCircle(double x, double y, double r) {
        System.out.printf("Circle(%.1f, %.1f) Radius: %.1f", x, y, r);
    }

    //method for printing info about a rectangle
    static void reportRectangle(double left, double top, double width, double height) {
        System.out.printf("Rectangle(%.1f, %.1f, %.1f, %.1f)", left, top, left+width, top-height);
    }

    /*method to find if point is in the circle. Calculates distance from center to point using a^2 + b^2 = c^2
    and then comparing that distance to the circles radius. Returns boolean true if point is in circle*/
    static boolean isPointInsideCircle(double ptX, double ptY, double circleX, double circleY, double circleRadius) {
        double distance = Math.sqrt(Math.pow(ptX -de circleX, 2) + Math.pow(ptY - circleY, 2));
        return distance <= circleRadius;
    }

    /*method to find if point is in the rectangle. Calculates distance from left side to point and compares that to the
    width of the rectangle and then does the same with the top and height. Returns boolean true if point is in rectangle*/
    static boolean isPointInsideRectangle(double ptX, double ptY, double rLeft, double rTop, double rWidth, double rHeight) {
        if (ptX - rLeft <= rWidth)
            return rTop - ptY <= rHeight;
        else
            return false;
    }

}

